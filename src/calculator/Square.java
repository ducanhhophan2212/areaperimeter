package calculator;

public class Square implements IShape{
    private double side;

    public Square(double side){
        this.side = side;
    }

    @Override
    public double getArea(){
        return side * side;
    }

    @Override
    public double getPerimeter(){
        return side * 4;
    }
}