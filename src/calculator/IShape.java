package calculator;

public interface IShape {
    double getArea();
    double getPerimeter();
}
