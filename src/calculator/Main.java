package calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        IShape[] shapes = new IShape[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Enter shape type (1 for Rectangle, 2 for Circle, 3 for Square): ");
            int shapeType = sc.nextInt();
            if (shapeType == 1) {
                System.out.println("Enter length of rectangle: ");
                double length = sc.nextDouble();
                System.out.println("Enter width of rectangle: ");
                double width = sc.nextDouble();
                shapes[i] = new Rectangle(length, width);
            } else if (shapeType == 2) {
                System.out.println("Enter radius of circle: ");
                double radius = sc.nextDouble();
                shapes[i] = new Circle(radius);
            } else if(shapeType == 3){
                System.out.println("Enter side of square: ");
                double side = sc.nextDouble();
                shapes[i] = new Square(side);
            } else {
                System.out.println("Invalid shape type.");
                i--;
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println("Area of shapes[" + i + "]: " + shapes[i].getArea());
            System.out.println("Perimeter of shapes[" + i + "]: " + shapes[i].getPerimeter());
        }
    }
}